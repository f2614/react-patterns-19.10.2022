import { useState } from "react";
import ChildComponent from "./ChildComponent";



const PropsInUseState = () => {
  const [parentState, setParentState] = useState(null)


  return ( <div>Props in useState


    <ChildComponent parentState={parentState} />
  </div> );
}
 
export default PropsInUseState;