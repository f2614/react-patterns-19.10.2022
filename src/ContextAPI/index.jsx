import { useState } from "react"
import FirstChildComponent from "./FirstChildComponent"
import TestContext from "./testContext"

const ContextAPIComponent = () => {
  const [value, setValue] = useState(null)
  

  return (
    <TestContext.Provider value={{ value, setValue }} >
      <FirstChildComponent/>
     </TestContext.Provider>
  )
}

export default ContextAPIComponent
