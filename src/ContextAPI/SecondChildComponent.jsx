import ThirdChildComponent from "./ThirdChildComponent";



const SecondChildComponent = ({ value, setValue }) => {
  return ( <div>
    <ThirdChildComponent value={value} setValue={setValue} />
  </div> );
}
 
export default SecondChildComponent;