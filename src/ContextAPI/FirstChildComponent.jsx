import SecondChildComponent from "./SecondChildComponent";



const FirstChildComponent = ({ value, setValue }) => {
  return ( <div>
    <SecondChildComponent value={value} setValue={setValue} />
  </div> );
}
 
export default FirstChildComponent;