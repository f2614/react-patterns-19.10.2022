import { useState } from "react";
import useFilters from "./useFilters";
import useRequest from "./useRequest";


const TestComponent = () => {

  // WITHOUT HOOK
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    setLoading(true)

    axios.get('/users')
      .then(res => setData(res.data))
      .catch(err => setError(err))
      .finally(() => setLoading(false))
  }, [])


  // WITH HOOK
  // const { data, loading, error } = useRequest('/users')
  // const { data, loading, error } = useRequest('/users')

  
  const { filters, filterChangeHandler, clearFilters, clearOrders } = useFilters('users', '3212df213123dfg23232dfdf2')


  return ( <div>Test component for Custom Hooks</div> );
}
 
export default TestComponent;