import { useState } from "react";

const IndexAsKeys = () => {
  const [todos, setTodos] = useState([])


  return ( <div>

    {
      todos.map((todo, index) => (
        <Todo todo={todo} key={todo.id} />
      ))
    }

  </div> );
}

export default IndexAsKeys;