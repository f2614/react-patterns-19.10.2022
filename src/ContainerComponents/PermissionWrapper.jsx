import { useMemo } from "react";



const PermissionWrapper = ({name, children}) => {
  const permissions = useSelector((state) => state.auth.permissions)
 
  const role = useSelector((state) => state.auth.roleInfo)


  const hasPermission = useMemo(() => {
    if(roke == 'admin') return true
    return permissions[name]
  }, [])

  if(hasPermission) return null

  return children;
}
 
export default PermissionWrapper;